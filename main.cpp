#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;

ifstream fin("test.in");
ofstream fout("test.out");

struct node {
    int x;
    int y;
    int g;
    int h;
    node*prev;
    node(int xx, int yy, int gg, int hh, node*p): x(xx), y(yy), g(gg), h(hh), prev(p) {}
};

int gdist(int x, int y, int startx, int starty) {
    return abs(x - startx) + abs(y - starty);
}

int hdist(int x, int y, int destx, int desty) {
    return abs(x - destx) + abs(y - desty);
}

bool inopen(int x, int y, vector<node*> open) {
    for (int i = 0; i < open.size(); ++i) {
        if (x == open[i]->x && y == open[i]->y)return true;
    }
    return false;
}

bool inclose(int x, int y, vector<node*> close) {
    for (int i = 0; i < close.size(); ++i) {
        if (x == close[i]->x && y == close[i]->y)return true;
    }
    return false;
}

node* returnnode(int x, int y, vector<node*> open) {
    for (int i = 0; i < open.size(); ++i) {
        if (x == open[i]->x && y == open[i]->y)return open[i];
    }
    return NULL;
}

bool canend(int destx, int desty, vector<node*> open) {
    for (int i = 0; i < open.size(); ++i) {
        if (destx == open[i]->x && desty == open[i]->y)return true;
    }
    return false;
}

int divide(vector<node*> a, int low, int high) {
    node* k = new node(a[low]->x, a[low]->y, a[low]->g, a[low]->h, a[low]->prev);
    do {
        while (low < high && (a[high]->h + a[high]->g) >= (k->h + k->g))--high;
        if (low < high) {
            a[low]->h = a[high]->h;
            a[low]->x = a[high]->x;
            a[low]->y = a[high]->y;
            a[low]->g = a[high]->g;
            a[low]->prev = a[high]->prev;
            ++low;
        }
        while (low < high && (a[low]->h + a[low]->g) <= (k->h + k->g))++low;
        if (low < high) {
            a[high]->h = a[low]->h;
            a[high]->x = a[low]->x;
            a[high]->y = a[low]->y;
            a[high]->g = a[low]->g;
            a[high]->prev = a[low]->prev;
            --high;
        }
    } while (low != high);
    a[low]->x = k->x;
    a[low]->y = k->y;
    a[low]->h = k->h;
    a[low]->g = k->g;
    a[low]->prev = k->prev;   //lastly low equals high
    return low;
}

void quicksort(vector<node*> a, int low, int high) {
    int mid;
    if (low >= high)return;
    mid = divide(a, low, high);
    quicksort(a, low, mid - 1);
    quicksort(a, mid + 1, high);
}

void quicksort(vector<node*> a, int num) {
    quicksort(a, 0, num - 1);
}

void astar(vector<vector<char> > maze, vector<node*> open, vector<node*> close, int startx, int starty, int destx, int desty, int row, int col, node*&ans) {
    while (open.size() > 0) {
        quicksort(open, open.size());
        node*tmp = open[0];
        open[0] = open[open.size() - 1];
        open.pop_back();
        close.push_back(tmp);
        if (maze[tmp->x + 1][tmp->y] != '%' && !inclose(tmp->x + 1, tmp->y, close)) {
            if (!inopen(tmp->x + 1, tmp->y, open)) {
                if (tmp->x + 1 == destx && tmp->y == desty) {
                    ans = new node(tmp->x + 1, tmp->y, tmp->g + 1, 0, tmp);
                    break;
                }
                node*newnode = new node(tmp->x + 1, tmp->y, tmp->g + 1, hdist(tmp->x + 1, tmp->y, destx, desty), tmp);
                open.push_back(newnode);
            } else {
                int dist1 = tmp->g + 1; // from tmp
                int dist2 = returnnode(tmp->x + 1, tmp->y, open)->g;
                if (dist1 < dist2)returnnode(tmp->x + 1, tmp->y, open)->prev = tmp; // from tmp, it takes more steps but less g
            }
        }
        /////////////////////////////
        if (maze[tmp->x - 1][tmp->y] != '%' && !inclose(tmp->x - 1, tmp->y, close)) {
            if (!inopen(tmp->x - 1, tmp->y, open)) {
                if (tmp->x - 1 == destx && tmp->y == desty) {
                    ans = new node(tmp->x - 1, tmp->y, tmp->g + 1, 0, tmp);
                    break;
                }
                node*newnode = new node(tmp->x - 1, tmp->y, tmp->g + 1, hdist(tmp->x - 1, tmp->y, destx, desty), tmp);
                open.push_back(newnode);
            } else {
                int dist1 = tmp->g + 1; // from tmp
                int dist2 = returnnode(tmp->x - 1, tmp->y, open)->g;
                if (dist1 < dist2)returnnode(tmp->x - 1, tmp->y, open)->prev = tmp; // from tmp, it takes more steps but less g
            }
        }
        ///////////////////////////////////
        if (maze[tmp->x][tmp->y + 1] != '%' && !inclose(tmp->x, tmp->y + 1, close)) {
            if (!inopen(tmp->x, tmp->y + 1, open)) {
                if (tmp->x == destx && tmp->y + 1 == desty) {
                    ans = new node(tmp->x, tmp->y + 1, tmp->g + 1, 0, tmp);
                    break;
                }
                node*newnode = new node(tmp->x, tmp->y + 1, tmp->g + 1, hdist(tmp->x, tmp->y + 1, destx, desty), tmp);
                open.push_back(newnode);
            } else {
                int dist1 = tmp->g + 1; // from tmp
                int dist2 = returnnode(tmp->x, tmp->y + 1, open)->g;
                if (dist1 < dist2)returnnode(tmp->x, tmp->y + 1, open)->prev = tmp; // from tmp, it takes more steps but less g
            }
        }
        ///////////////////////////
        if (maze[tmp->x][tmp->y - 1] != '%' && !inclose(tmp->x, tmp->y - 1, close)) {
            if (!inopen(tmp->x, tmp->y - 1, open)) {
                if (tmp->x == destx && tmp->y - 1 == desty) {
                    ans = new node(tmp->x, tmp->y - 1, tmp->g + 1, 0, tmp);
                    break;
                }
                node*newnode = new node(tmp->x, tmp->y - 1, tmp->g + 1, hdist(tmp->x, tmp->y - 1, destx, desty), tmp);
                open.push_back(newnode);
            } else {
                int dist1 = tmp->g + 1; // from tmp
                int dist2 = returnnode(tmp->x, tmp->y - 1, open)->g;
                if (dist1 < dist2)returnnode(tmp->x, tmp->y - 1, open)->prev = tmp; // from tmp, it takes more steps but less g
            }
        }
    }
}

int main() {
    //read the matrix
    string buff;
    vector<vector<char> > maze(100, vector<char>(100));
    int col;
    int row = 0;
    while (!fin.eof()) {
        getline(fin, buff);
        col = buff.length();
        for (int i = 0; i < col; ++i) {
            maze[row][i] = buff[i];
        }
        row++;
    }
    vector<vector<bool> > visited(row, vector<bool>(col));
    //find the start
    int startx, starty;
    int destx, desty;
    for (int i = 0; i < row; ++i) {
        for (int j = 0; j < col; ++j) {
            if (maze[i][j] == '.') {
                startx = i;
                starty = j;
            }
            if (maze[i][j] == 'P') {
                destx = i;
                desty = j;
            }
        }
    }
    for (int i = 0; i < row; ++i) {
        for (int j = 0; j < col; ++j)visited[i][j] = false;
    }
    maze[startx][starty] = ' ';

    //dfs
    vector<node*> open;
    vector<node*> close;
    node*startnode = new node(startx, starty, 0, hdist(startx, starty, destx, desty), NULL);
    open.push_back(startnode);
    node*ans = NULL;
    astar(maze, open, close, startx, starty, destx, desty, row, col, ans);

    while (ans != NULL) {
        maze[ans->x][ans->y] = '.';
        ans = ans->prev;
    }

    for (int i = 0; i < row; ++i) {
        for (int j = 0; j < col; ++j) {
            fout << maze[i][j];
        }
        fout << endl;
    }
    return 0;
}
