
main: main.cpp
	g++ main.cpp -o main

test.out: main test.in
	./main
	@cat test.out

myversion: myversion.cpp
	@g++ -g --std=c++11 myversion.cpp -o myversion

clean:
	@rm -f a.out main test.out myversion

.PHONY: clean
