#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include <unordered_set>
#include <cmath>
#include <algorithm>

using namespace std;

ifstream fin("test.in");

struct Node {
    int x, y;
    int f, g, h;
    struct Node* parent;
    Node(int x, int y): x(x), y(y), f(0), g(0), h(0), parent(NULL) {
    }
    Node* setG(int g) {
        this->g = g;
        return this;
    }
    Node* setH(int endx, int endy) {
        this->h = abs(this->x - endx) + abs(this->y - endy);
        return this;
    }
    Node* setF() {
        this->f = this->g + this->h;
        return this;
    }
    Node* setParent(Node* parent) {
        this->parent = parent;
        return this;
    }
};

typedef struct Node Node;

struct Compare {
    bool operator()(struct Node* p1, struct Node* p2) {
        return p1->f < p2->f;
    }
} compare;

const Node* AStar(vector<vector<char> >& maze, vector<vector<int> >& mazeStatus,
                  int row, int col, int endx, int endy, vector<Node*>& openlist, vector<Node*>& closelist) {
    int x, y;
    static const vector<vector<int> > v = {{ -1, 0}, { +1, 0}, {0, -1}, {0, 1}};
    while (!openlist.empty()) {
        sort(openlist.begin(), openlist.end(), compare);
        Node* tmp = openlist[0];
        openlist[0] = openlist[openlist.size() - 1];
        openlist.pop_back();
        for (auto pos : v) {
            x = pos[0] + tmp->x;
            y = pos[1] + tmp->y;
            if (x < 0 || x > row - 1 || y < 0 || y > col - 1 || maze[x][y] == '%') continue;
            if (x == endx && y == endy) {
                return tmp;
            }
            if (mazeStatus[x][y] == -1) { // not checked
                Node* curNode = new Node(x, y);
                curNode->setG(tmp->g + 1)->setH(endx, endy)->setF()->setParent(tmp);
                openlist.push_back(curNode);
                mazeStatus[x][y] = 0;
            } else if (mazeStatus[x][y] == 0) { // in openlist, then update if needed
                Node* curNode = NULL;
                for (int i = 0; i < openlist.size(); i++) {
                    if (openlist[i]->x == x && openlist[i]->y == y) {
                        curNode = openlist[i];
                        break;
                    }
                }
                if (tmp->g + 1 < curNode->g) {
                    curNode->setG(tmp->g + 1)->setF()->setParent(tmp);
                }
            }
        }
        closelist.push_back(tmp);
        mazeStatus[tmp->x][tmp->y] = 1;
    }
    return NULL;
}

int main(int argc, char *argv[]) {
    string buff;
    vector<vector<char> > maze;
    while (!fin.eof()) {
        getline(fin, buff);
        maze.push_back(vector<char>(buff.begin(), buff.end()));
    }
    if (maze.size() == 0) {
        cout << "maze is empty" << endl;
        return 0;
    }
    //
    int row = maze.size(), col = maze[0].size();
    int startx, starty, endx, endy;
    for (int i = 0; i < row; i++) {
        for (int j = 0; j < col; j++) {
            if (maze[i][j] == '.') {
                startx = i;
                starty = j;
            } else if (maze[i][j] == 'P') {
                endx = i;
                endy = j;
            }
        }
    }
    // Initialize
    vector<vector<int> > mazeStatus(row, vector<int>(col, -1)); // -1: not visited, 0: in openlist, 1: in close list
    mazeStatus[startx][starty] = 0;
    vector<Node*> openlist = {(new Node(startx, starty))->setG(0)->setH(endx, endy)->setF()};
    vector<Node*> closelist;
    // Search
    const Node* p = AStar(maze, mazeStatus, row, col, endx, endy, openlist, closelist);
    // Print result, no solutiion if p == NULL
    while (p != NULL) {
        maze[p->x][p->y] = '.';
        p = p->parent;
    }
    for (int i = 0; i < row; i++) {
        for (int j = 0; j < col; j++) {
            cout << maze[i][j];
        }
        cout << endl;
    }
    // clear
    for (auto pt : openlist) delete pt;
    for (auto pt : closelist) delete pt;
    return 0;
}
